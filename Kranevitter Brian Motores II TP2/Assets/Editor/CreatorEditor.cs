﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ForestCreator))]
public class CreatorEditor : Editor
{
    ForestCreator myBase;
    Roulette roulette;
    Texture2D _logo;

    private void OnEnable()
    {
        myBase = (ForestCreator)target;
        roulette = new Roulette();
        _logo = (Texture2D)Resources.Load("logo2", typeof(Texture2D));

        if (myBase.rectangleVertex == null)
        {
            myBase.rectangleVertex = GameObject.Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube), myBase.transform.position + new Vector3(100, 0, 100), myBase.transform.rotation).transform;
            myBase.rectangleVertex.parent = myBase.transform;
            myBase.rectangleVertex.name = "Opposite Rectangle Vertex";
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();


        if (myBase.chances.Count > myBase.trees.Count)
            myBase.chances.RemoveAt(myBase.chances.Count - 1);
        else if (myBase.chances.Count < myBase.trees.Count)
            myBase.chances.Add(0);

        for (int i = 0; i < myBase.chances.Count; i++)
        {
            myBase.chances[i] = EditorGUILayout.IntSlider("Tree N" + i + " chance", myBase.chances[i], 0, 100);
        }

        /* if (myBase.trees.Count > 0)
         {
             myBase.chances.Clear();
             int[] newRange = new int[myBase.trees.Count];
             myBase.chances.AddRange(newRange);
             for (int i = 0; i < myBase.trees.Count; i++)
             {
                 myBase.chances[i] = EditorGUILayout.IntSlider("Tree N" + i + " chance", myBase.chances[i], 0, 100);
             }
         }*/

        if (GUILayout.Button("Generate"))
        {
            GameObject parent = GameObject.CreatePrimitive(PrimitiveType.Cube);
            parent.transform.position = myBase.transform.position;
            myBase.forestCreated.Add(parent);

            Vector2 newForestSize = new Vector2(myBase.rectangleVertex.transform.position.x - myBase.transform.position.x, myBase.rectangleVertex.transform.position.z - myBase.transform.position.z);


            List<Vector2> points = PoissonDiscSampling.GeneratePoints(myBase.distance, newForestSize, myBase.accuracy);

            for (int i = 0; i < points.Count; i++)
            {
                RaycastHit hit;
                if (Physics.Raycast(myBase.transform.position + new Vector3(points[i].x, 0, points[i].y), Vector3.down, out hit, int.MaxValue, myBase.layers))
                {
                    Dictionary<int, int> dic = new Dictionary<int, int>();
                    for (int c = 0; c < myBase.chances.Count; c++)
                    {
                        dic.Add(c, myBase.chances[c]);
                    }
                    int randomNumber = roulette.Run(dic);
                    GameObject child = GameObject.Instantiate(myBase.trees[randomNumber], hit.point, Quaternion.identity);
                    float scaleRandomizer = Random.Range(1, 1 + myBase.scaleRandomizer);
                    child.transform.localScale = child.transform.localScale * myBase.scale * scaleRandomizer;

                    child.transform.parent = parent.transform;
                }
            }
            /*
            for (int i = 0; i < myBase.rows; i++)
            {
                for (int b = 0; b < myBase.columns; b++)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(myBase.transform.position + new Vector3(i * myBase.distance * (1 + Random.Range(-myBase.distanceRandomizer / 2, myBase.distanceRandomizer / 2)),
                        0, b * myBase.distance * (1 + Random.Range(-myBase.distanceRandomizer / 2, myBase.distanceRandomizer / 2))), Vector3.down, out hit, int.MaxValue, myBase.layers))
                    {
                        Dictionary<int, int> dic = new Dictionary<int, int>();
                        for (int c = 0; c < myBase.chances.Count; c++)
                        {
                            dic.Add(c, myBase.chances[c]);
                        }
                        int randomNumber = roulette.Run(dic);
                        GameObject child = GameObject.Instantiate(myBase.trees[randomNumber], hit.point, Quaternion.identity);
                        float scaleRandomizer = Random.Range(1, 1 + myBase.scaleRandomizer);
                        child.transform.localScale = child.transform.localScale * myBase.scale * scaleRandomizer;

                        child.transform.parent = parent.transform;
                    }
                }
            }*/
        }
        if (myBase.forestCreated.Count > 0)
        {
            if (GUILayout.Button("Undo"))
            {
                DestroyImmediate(myBase.forestCreated[myBase.forestCreated.Count - 1]);
                myBase.forestCreated.RemoveAt(myBase.forestCreated.Count - 1);
            }
        }
    }
    void OnSceneGUI()
    {
        if (myBase.rectangleVertex == null)
            return;

        Vector3 pos = myBase.transform.position;

        Vector3[] verts = new Vector3[4];
        if (myBase.rectangleVertex.localPosition.x >= 0 && myBase.rectangleVertex.localPosition.z >= 0)
            verts = new Vector3[] { new Vector3(pos.x, pos.y, pos.z), new Vector3(pos.x, pos.y, pos.z + myBase.rectangleVertex.localPosition.z),
                new Vector3(pos.x + myBase.rectangleVertex.localPosition.x, pos.y, pos.z + myBase.rectangleVertex.localPosition.z),
                new Vector3(pos.x + myBase.rectangleVertex.localPosition.x, pos.y, pos.z) };

        if (myBase.rectangleVertex.localPosition.x <= 50)
            myBase.rectangleVertex.localPosition = new Vector3(50, myBase.rectangleVertex.localPosition.y, myBase.rectangleVertex.localPosition.z);

        if (myBase.rectangleVertex.localPosition.z <= 50)
            myBase.rectangleVertex.localPosition = new Vector3(myBase.rectangleVertex.localPosition.x, myBase.rectangleVertex.localPosition.y, 50);

        if (myBase.rectangleVertex.localPosition.y != 0)
            myBase.rectangleVertex.localPosition = new Vector3(myBase.rectangleVertex.localPosition.x, 0, myBase.rectangleVertex.localPosition.z);

        myBase.rectangleVertex.position = Handles.DoPositionHandle(myBase.rectangleVertex.position, myBase.rectangleVertex.rotation); //Pasarle un Vector3 para que no haga falta usar un gameObject
        //Handle central para la posición.
        Handles.DrawSolidRectangleWithOutline(verts, new Color(0.5f, 0.5f, 0.5f, 0.1f), new Color(0, 0, 0, 1));
    }
}
