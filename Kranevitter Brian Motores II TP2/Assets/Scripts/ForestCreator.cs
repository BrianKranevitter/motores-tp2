﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestCreator : MonoBehaviour
{
    public int accuracy;
    public float distance, scale, scaleRandomizer;
    public Transform rectangleVertex;
    public Vector2 forestSize;
    public List<GameObject> trees;
    [HideInInspector]
    public List<int> chances = new List<int>();
    [HideInInspector]
    public List<GameObject> forestCreated = new List<GameObject>();
    public LayerMask layers;
}
